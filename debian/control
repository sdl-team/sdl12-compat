Source: sdl12-compat
Priority: optional
Maintainer: Debian SDL packages maintainers <pkg-sdl-maintainers@lists.alioth.debian.org>
Uploaders:
 Simon McVittie <smcv@debian.org>,
 Stephen Kitt <skitt@debian.org>,
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 libglu1-mesa-dev,
 libsdl2-dev,
 libx11-dev,
Standards-Version: 4.6.2
Section: libs
Homepage: https://github.com/libsdl-org/sdl12-compat
Vcs-Git: https://salsa.debian.org/sdl-team/sdl12-compat.git
Vcs-Browser: https://salsa.debian.org/sdl-team/sdl12-compat
Rules-Requires-Root: no

Package: libsdl1.2-compat
Section: oldlibs
Architecture: any
Multi-Arch: same
Depends:
 libsdl1.2debian (>= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Pre-Depends:
 ${misc:Pre-Depends},
Description: SDL 1.2 binary compatibility library - transitional package
 sdl12-compat provides a binary compatible API for programs built
 against SDL 1.2, but using SDL 2.0.
 .
 If you are writing new code, please target SDL 2.0 directly instead
 of using this layer.
 .
 This transitional package provides symbolic links to the library in a private
 location, matching the way it was used before it was Debian's default
 implementation of the SDL 1.2 ABI.

Package: libsdl1.2-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libgl-dev,
 libglu1-mesa-dev,
 libsdl1.2debian (= ${binary:Version}),
 libsdl2-dev,
 pkgconf,
 ${misc:Depends},
 ${shlibs:Depends},
Pre-Depends:
 ${misc:Pre-Depends},
Provides:
 libsdl-dev (= ${binary:Version}),
 libsdl1.2-compat-dev (= ${binary:Version}),
Breaks:
 libsdl-dev (<< ${sdl12compat:Takeover-Version}~),
 libsdl1.2-compat-dev (<< ${sdl12compat:Takeover-Version}~),
Replaces:
 libsdl-dev (<< ${sdl12compat:Takeover-Version}~),
 libsdl1.2-compat-dev (<< ${sdl12compat:Takeover-Version}~),
Description: SDL 1.2 binary compatibility library - development files
 sdl12-compat provides a binary compatible API for programs built
 against SDL 1.2, but using SDL 2.0.
 .
 If you are writing new code, please target SDL 2.0 directly instead
 of using this layer.
 .
 This package provides development files.

Package: libsdl1.2debian
Architecture: any
Multi-Arch: same
Depends:
 libsdl2-2.0-0,
 ${misc:Depends},
 ${shlibs:Depends},
Pre-Depends:
 ${misc:Pre-Depends},
Provides:
 libsdl-1.2-0 (= ${binary:Version}),
 libsdl1.2-compat-shim (= ${binary:Version}),
Breaks:
 libsdl1.2-compat (<< ${sdl12compat:Takeover-Version}~),
 libsdl1.2-compat-shim (<< ${sdl12compat:Takeover-Version}~),
Replaces:
 libsdl1.2-compat (<< ${sdl12compat:Takeover-Version}~),
 libsdl1.2-compat-shim (<< ${sdl12compat:Takeover-Version}~),
Description: SDL 1.2 binary compatibility library wrapping SDL 2.0
 This package contains sdl12-compat, a binary compatible API for programs built
 against SDL 1.2, but using SDL 2.0.
 .
 This package replaces the classic SDL 1.2 library with the SDL 2.0
 compatibility layer, so that all SDL 1.2 program will use it.
 .
 If you are writing new code, please target SDL 2.0 directly instead
 of using this layer.

Package: libsdl1.2-compat-dev
Section: oldlibs
Architecture: any
Multi-Arch: same
Depends:
 libsdl1.2-dev (>= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Pre-Depends:
 ${misc:Pre-Depends},
Description: SDL 1.2 binary compatibility library - transitional development files
 sdl12-compat provides a binary compatible API for programs built
 against SDL 1.2, but using SDL 2.0.
 .
 If you are writing new code, please target SDL 2.0 directly instead
 of using this layer.
 .
 This transitional package depends on a version of libsdl1.2-dev
 new enough that it actually contains sdl12-compat development files.

Package: libsdl1.2-compat-shim
Section: oldlibs
Architecture: any
Multi-Arch: same
Depends:
 libsdl1.2debian (>= ${binary:Version}),
 libsdl1.2-compat (>= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: libsdl1.2debian replacement using SDL 2.0 - transitional package
 sdl12-compat provides a binary compatible API for programs built
 against SDL 1.2, but using SDL 2.0.
 .
 If you are writing new code, please target SDL 2.0 directly instead
 of using this layer.
 .
 This transitional package depends on a version of libsdl1.2debian
 new enough that it actually contains sdl12-compat.

Package: libsdl1.2-compat-tests
Section: misc
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Pre-Depends:
 ${misc:Pre-Depends},
Description: SDL 1.2 binary compatibility library - tests
 libsdl1.2-compat provides a binary compatible API for programs built
 against SDL 1.2, but using SDL 2.0.
 .
 If you are writing new code, please target SDL 2.0 directly instead
 of using this layer.
 .
 This package provides manual test programs.
